import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { FlightService } from './service/flight.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  title = 'flights-search';
  flightForm = this.fb.group({
    fromCity: ['', Validators.required],
    toCity: ['', Validators.required]
  });
  origin = [];
  destination = [];
  flights = [];
  flightList = [];
  submitted = false;
  selectedPrice = 0;
  isFlight = true;
  constructor(private fb: FormBuilder, private flightService: FlightService) { }
  ngOnInit() {
    this.flightService.getCities().subscribe(data => {
      this.origin = data.origin;
      this.destination = data.destination
    });
  }
  changeSource(event) {
    this.flightForm.get('fromCity').setValue(event.target.value, {
      onlySelf: true
    })
  }
  changeDestination(event) {
    this.flightForm.get('toCity').setValue(event.target.value, {
      onlySelf: true
    })
  }
  search() {
    this.submitted = true;
    this.isFlight = true;
    if (!this.flightForm.invalid) {
      const selectedFrom = this.flightForm.get('fromCity').value;
      const selectedTo = this.flightForm.get('toCity').value;
      this.flightService.getFlights().subscribe(data => {
        this.flights = data.flightList.filter((item) => {
          if (item.origin == selectedFrom && item.destination == selectedTo) {
            return item;
          }
        })
        this.flightList = this.flights;
        if (this.flights.length < 1) {
          this.isFlight = false;
        }
      });
    }
  }

  priceChange() {
    if (this.submitted) {
      this.flights = this.flightList.filter((item) => {
        if (item.fare <= this.selectedPrice) {
          this.isFlight = true;
          return item;
        } else {
          this.isFlight = false;
        }
      })
    }
  }
}
