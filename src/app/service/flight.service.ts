import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class FlightService {

  constructor(private http: HttpClient) { }

  getCities(): Observable<any> {
    return this.http.get<any>('https://demo2041410.mockable.io/cities').pipe(
      map(data => data.payload),
      tap(data => console.log("Fetched cities")),
      catchError(this.handleError)
    )
  }

  getFlights(): Observable<any> {
    return this.http.get<any>('https://demo2041410.mockable.io/flights').pipe(
      map(data => data.payload),
      tap(data => console.log("Fetched flights")),
      catchError(this.handleError)
    )
  }

  private handleError(error: HttpErrorResponse) {
    let errorMsg = '';
    if(error.error instanceof ErrorEvent) {
      errorMsg = `Error : ${error.error.message}`;
    } else {
      errorMsg = `Error Code: ${error.status}, Error Message: ${error.message}`;
    }
    return throwError(errorMsg);
  }
}
