# FlightsSearch

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.5.

## To add Angular material

Run `ng add @angular/material`

## To run the project

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. 

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `npm run e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

